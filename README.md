# Éditeur de données PasseGares

[![pipeline status](https://framagit.org/JonathanMM/passegares-editeur/badges/master/pipeline.svg)](https://framagit.org/JonathanMM/passegares-editeur/commits/master)
[![coverage report](https://framagit.org/JonathanMM/passegares-editeur/badges/master/coverage.svg)](https://framagit.org/JonathanMM/passegares-editeur/commits/master)

## Présentation

Ce projet est un éditeur de données pour [PasseGares](https://framagit.org/JonathanMM/passegares), application android pour tamponner toutes les gares visitées

## Comment ça marche ?

Ce projet est un projet [Create React App](https://github.com/facebook/create-react-app). C'est donc une application react. Une démonstration est disponible sur http://passegares.nocle.fr (les données ne sont pas forcement à jour).

## Installation en local

En local, les commandes disponibles sont :

- `npm start` pour lancer le serveur et accéder à l'éditeur (par défaut [http://localhost:3000](http://localhost:3000)).
- `npm test` pour lancer les tests unitaires.
- `npm run build` pour compiler une version statique de l'éditeur.

Pour que cela fonctionne, il est nécessaire de commencer par faire un `npm install` afin que toutes les dépendances soient installés, et de créer un dossier data/ dans le dossier public/, afin d'y mettre les données (contenu de passegares dans app/src/main/assets/).

## Utilisation

Commencer par charger les données. Choissiez une région (le système ne fonctionne pour le moment qu'en modifiant une région à la fois), puis modifier les données.

Une fois satisfait, cliquez sur Générer les fichiers, puis télécharger les trois fichiers. Une nouvelle version de métadonnées aura été créé automatiquement. Pour faire les modifications en plusieurs fois, vous pouvez enregistrer ces fichiers dans public/data/, afin qu'ils soient utilisés au prochain chargement.

N'oubliez pas de faire une merge request sur le projet PasseGares, afin que vos données soient insérés dans le projet.

## Contribution

Cette application est ouverte aux contributions.

## Crédits

Les logos et sources de données appartiennent à leur propriétaire respectifs, et ne sont pas compris dans le cadre de la licence de ce logiciel.

## Contact

Vous pouvez poster une requête de bug, ou envoyer un courriel à :
passegares[@]nocle.fr
