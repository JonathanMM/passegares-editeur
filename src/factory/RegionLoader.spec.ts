import { RegionLoader } from "./RegionLoader";
import each from "jest-each";

describe("RegionLoader", () => {
  it("the Region file is empty", () => {
    let loader = new RegionLoader();

    let sut = loader.load("");

    expect(sut.regions.length).toBe(0);
  });

  it("the Region file contains one region", () => {
    let loader = new RegionLoader();

    let sut = loader.load(
      "id;nom;dossierId;vCreation;vMaj\n1;Île-de-France;Paris;1;0"
    );

    expect(sut.regions.length).toBe(1);
    expect(sut.regions[0].id).toBe(1);
    expect(sut.regions[0].nom).toBe("Île-de-France");
    expect(sut.regions[0].dossierId).toBe("Paris");
    expect(sut.regions[0].loaded).toBeFalsy();
    expect(sut.regions[0].versionCreation.numero).toBe(1);
    expect(sut.regions[0].versionCreation.existe).toBeTruthy();
    expect(sut.regions[0].versionMaj.numero).toBe(0);
    expect(sut.regions[0].versionMaj.existe).toBeTruthy();
  });

  each([
    "id;nom;dossierId;vCreation;vMaj\nBonjour",
    "id;nom;dossierId\n1;Île-de-France;Paris;1;0",
    "id,nom,dossierId,vCreation,vMaj\n1,Île-de-France,Paris,1,0"
  ]).it("the Region file is incorrect", (content: string) => {
    let loader = new RegionLoader();

    expect(() => loader.load(content)).toThrow();
  });
});
