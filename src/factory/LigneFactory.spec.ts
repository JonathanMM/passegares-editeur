import { IVersionManager } from "./VersionManager";
import Version from "../structure/Version";
import { LigneFactory } from "./LigneFactory";
import { TypeLigne } from "../structure/TypeLigne";

describe("LigneFactory", () => {
  it("generate returns the right line", () => {
    let mockVersion: Version = new Version(1, true);
    let mockVersionManager: IVersionManager = {
      versions: {},
      getVersion: (id: number) => mockVersion,
      getNoneVersion: () => mockVersion,
      getWorkspaceVersion: () => mockVersion,
      getNewNumeroVersion: () => 1
    };

    let idExterneFoo = "idExterneFoo";
    let nomFoo = "nomFoo";
    let couleurFoo = "couleurFoo";
    let ordreFoo = 42;

    let factory = new LigneFactory(mockVersionManager);

    let sut = factory.generate(
      idExterneFoo,
      nomFoo,
      TypeLigne.Funiculaire,
      couleurFoo,
      ordreFoo
    );

    expect(sut.idExterne).toBe(idExterneFoo);
    expect(sut.nom).toBe(nomFoo);
    expect(sut.type).toBe(TypeLigne.Funiculaire);
    expect(sut.couleur).toBe(couleurFoo);
    expect(sut.ordre).toBe(ordreFoo);
  });
});
