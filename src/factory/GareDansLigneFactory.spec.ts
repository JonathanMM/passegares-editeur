import { IVersionManager } from "./VersionManager";
import Version from "../structure/Version";
import { TypeLigne } from "../structure/TypeLigne";
import Gare from "../structure/Gare";
import Ligne from "../structure/Ligne";
import { Couleur } from "../structure/Couleur";
import { GareDansLigneFactory } from "./GareDansLigneFactory";
import { Fond, Point } from "../structure/PointDansLigne";

describe("GareDansLigneFactory", () => {
  it("generate returns the right gdl", () => {
    let mockVersion: Version = new Version(1, true);
    let mockVersionManager: IVersionManager = {
      versions: {},
      getVersion: (id: number) => mockVersion,
      getNoneVersion: () => mockVersion,
      getWorkspaceVersion: () => mockVersion,
      getNewNumeroVersion: () => 1
    };
    let foo = "foo";

    let gare = new Gare(
      foo,
      foo,
      foo,
      foo,
      0,
      0,
      Couleur.Cyan,
      Couleur.Cyan,
      mockVersion,
      mockVersion,
      mockVersion
    );
    let ligne = new Ligne(
      foo,
      foo,
      TypeLigne.Funiculaire,
      0,
      foo,
      mockVersion,
      mockVersion,
      mockVersion
    );

    let factory = new GareDansLigneFactory(mockVersionManager);

    let sut = factory.generate(gare, ligne);

    expect(sut.ligne).toBe(ligne);
    expect(sut.gare).toBe(gare);
    expect(sut.ordre).toBe(0);
    expect(sut.pdlFond).toBe(Fond.Vide);
    expect(sut.pdlPoint).toBe(Point.Vide);
  });
});
