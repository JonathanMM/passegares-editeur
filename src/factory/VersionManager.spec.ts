import VersionManager from "./VersionManager";

describe("VersionManager", () => {
  it("Workspace version doesn't exist", () => {
    let versionManager = new VersionManager();

    let sut = versionManager.getWorkspaceVersion();

    expect(sut.existe).toBeFalsy();
  });

  it("None version exists", () => {
    let versionManager = new VersionManager();

    let sut = versionManager.getNoneVersion();

    expect(sut.existe).toBeTruthy();
  });

  describe("the getVersion method", () => {
    let versionManager = new VersionManager();

    it("throws an error is NaN is provided", () => {
      expect(() => versionManager.getVersion(NaN)).toThrowError();
    });

    it("returns the right version", () => {
      let versionNumber = 42;

      let sut = versionManager.getVersion(versionNumber);

      expect(sut.numero).toBe(versionNumber);
      expect(sut.existe).toBeTruthy();
    });

    it("returns again the right version", () => {
      let versionNumber = 42;

      let sut = versionManager.getVersion(versionNumber);

      expect(sut.numero).toBe(versionNumber);
      expect(sut.existe).toBeTruthy();
    });
  });

  describe("the getNewNumeroVersion method", () => {
    it("returns 1 if there are no version", () => {
      let versionManager = new VersionManager();

      let sut = versionManager.getNewNumeroVersion();

      expect(sut).toBe(1);
    });

    it("returns max+1 if there are versions", () => {
      let versionManager = new VersionManager();

      //Generate versions
      versionManager.getVersion(10);
      versionManager.getVersion(42);

      let sut = versionManager.getNewNumeroVersion();

      expect(sut).toBe(43);
    });
  });
});
