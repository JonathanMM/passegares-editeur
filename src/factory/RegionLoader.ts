import Region from "../structure/Region";
import Version from "../structure/Version";
import { RegionRepository } from "../structure/RegionRepository";

export class RegionLoader {
  public load(content: string): RegionRepository {
    let lignes = content.split("\n");
    let i = 0;
    let pos: { [nom: string]: number } = {};
    let regions: Array<Region> = new Array<Region>();
    for (let ligne of lignes) {
      let cellules = ligne.split(";");
      if (ligne === "" || cellules.length === 0) continue;
      if (i === 0) {
        //Entêtes
        cellules.forEach((value: string, index: number) => {
          pos[value.trim()] = index;
        });
      } else {
        //Data
        regions.push(
          new Region(
            parseInt(cellules[pos["id"]].trim()),
            cellules[pos["nom"]].trim(),
            cellules[pos["dossierId"]].trim(),
            new Version(parseInt(cellules[pos["vCreation"]].trim()), true),
            new Version(parseInt(cellules[pos["vMaj"]].trim()), true)
          )
        );
      }
      i++;
    }
    return new RegionRepository(regions);
  }
}
