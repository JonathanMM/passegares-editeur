import React, { Component } from "react";
import LigneItem from "./LigneItem";
import Ligne from "../../../structure/Ligne";
import AjouterLigneForm from "./AjouterLigneForm";
import { ActionRegion, TypeActionRegion } from "../../ActionRegion";
import autobind from "autobind-decorator";

interface AfficherLignesProps {
  lignes: Array<Ligne>;
  selectedLignes: Array<Ligne>;
  onSelectedLigne: (ligne: Ligne) => () => void;
  dispatcherRegion: React.Dispatch<ActionRegion>;
}

export default class AfficherLignes extends Component<AfficherLignesProps> {
  @autobind
  selectLigne(ligne: Ligne, selected: boolean) {
    this.props.dispatcherRegion({ type: TypeActionRegion.setSelectionLigne, ligne, selected });
  }

  render() {
    return (
      <div>
        <h2>Lignes</h2>
        <table>
          <thead>
            <tr>
              <td></td>
              <th>Id</th>
              <th>Nom</th>
              <th>Type</th>
              <th>Couleur</th>
              <th>Ordre</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.props.lignes.map((ligne: Ligne) => (
              <LigneItem
                ligne={ligne}
                key={ligne.idExterne}
                afficherGares={this.props.onSelectedLigne(ligne)}
                selected={this.props.selectedLignes.indexOf(ligne) !== -1}
                onSelectedLigne={this.selectLigne}
              />
            ))}
          </tbody>
        </table>
        <AjouterLigneForm dispatcherRegion={this.props.dispatcherRegion} />
      </div>
    );
  }
}
