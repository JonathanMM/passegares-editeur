export function orderByValue(a: any, b: any): number {
  return a < b ? -1 : a > b ? 1 : 0;
}
