import Region from "./Region";

export class RegionRepository {
  regions: Array<Region>;

  constructor(regions: Array<Region>) {
    this.regions = regions;
  }
}
