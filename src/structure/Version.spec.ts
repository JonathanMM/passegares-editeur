import Version from "./Version";

describe("Version", () => {
  it("version number is incorrect", () => {
    expect(() => new Version(NaN, true)).toThrow();
  });
});
