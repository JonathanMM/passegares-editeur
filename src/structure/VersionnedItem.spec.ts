import { VersionnedItem } from "./VersionnedItem";
import Version from "./Version";

class VersionnedItemTest extends VersionnedItem {}

describe("Versionned Item", () => {
  it("isDeleted is true when vSuppression is not equal to 0", () => {
    let emptyVersion = new Version(0, true);
    let suppVersion = new Version(42, true);

    let sut = new VersionnedItemTest(emptyVersion, emptyVersion, suppVersion);

    expect(sut.isDeleted()).toBeTruthy();
  });

  it("isDeleted is false when vSuppression is equal to 0", () => {
    let notEmptyVersion = new Version(12, true);
    let suppVersion = new Version(0, true);

    let sut = new VersionnedItemTest(
      notEmptyVersion,
      notEmptyVersion,
      suppVersion
    );

    expect(sut.isDeleted()).toBeFalsy();
  });
});
