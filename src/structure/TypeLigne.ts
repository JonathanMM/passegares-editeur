export enum TypeLigne {
  Metro,
  Tramway,
  Train,
  Trains,
  Navette,
  RER,
  Funiculaire
}
