export interface ExportedData {
  fileGares: string;
  fileLignes: string;
  fileGaresDansLigne: string;
  selection?: {
    fileGares: string;
    fileLignes: string;
    fileGaresDansLigne: string;
  };
}
