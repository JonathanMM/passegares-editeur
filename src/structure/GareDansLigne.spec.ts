import GareDansLigne from "./GareDansLigne";
import Gare from "./Gare";
import Version from "./Version";
import { Couleur } from "./Couleur";
import Ligne from "./Ligne";
import { TypeLigne } from "./TypeLigne";

describe("GareDansLigne", () => {
  it("withOrdre should be modified order", () => {
    let foo = "foo";
    let pdlFond = 1;
    let pdfPoint = 2;
    let fooVersion1 = new Version(1, true);
    let fooVersion2 = new Version(1, true);
    let fooVersion3 = new Version(1, true);

    let gare = new Gare(
      foo,
      foo,
      foo,
      foo,
      0,
      0,
      Couleur.Cyan,
      Couleur.Cyan,
      fooVersion1,
      fooVersion1,
      fooVersion1
    );
    let ligne = new Ligne(
      foo,
      foo,
      TypeLigne.Funiculaire,
      0,
      foo,
      fooVersion1,
      fooVersion1,
      fooVersion1
    );

    let gdl = new GareDansLigne(
      gare,
      ligne,
      null,
      null,
      null,
      1,
      pdlFond,
      pdfPoint,
      fooVersion1,
      fooVersion2,
      fooVersion3
    );

    let sut = gdl.withOrdre(42);

    expect(sut.ordre).toBe(42);
    expect(sut.ligne).toBe(ligne);
    expect(sut.gare).toBe(gare);
    expect(sut.pdlFond).toBe(pdlFond);
    expect(sut.pdlPoint).toBe(pdfPoint);
    expect(sut.versionCreation).toBe(fooVersion1);
    expect(sut.versionMaj).toBe(fooVersion2);
    expect(sut.versionSuppression).toBe(fooVersion3);
  });
});
