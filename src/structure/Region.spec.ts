import Region from "./Region";
import Version from "./Version";
import Gare from "./Gare";
import { Couleur } from "./Couleur";
import Ligne from "./Ligne";
import { TypeLigne } from "./TypeLigne";
import GareDansLigne from "./GareDansLigne";

describe("Region", () => {
  let foo: string;
  let bar: string;
  let fooVersion: Version;
  let gare: Gare;
  let ligne: Ligne;
  let gdl: GareDansLigne;

  beforeAll(() => {
    let fooIdLigne = "fooIdLigne";
    let fooIdGare = "fooIdGare";
    foo = "foo";
    bar = "bar";
    fooVersion = new Version(1, true);
    gare = new Gare(
      fooIdGare,
      fooIdGare,
      foo,
      foo,
      0,
      0,
      Couleur.Cyan,
      Couleur.Cyan,
      fooVersion,
      fooVersion,
      fooVersion
    );
    ligne = new Ligne(
      fooIdLigne,
      foo,
      TypeLigne.Funiculaire,
      0,
      foo,
      fooVersion,
      fooVersion,
      fooVersion
    );
    gdl = new GareDansLigne(
      gare,
      ligne,
      null,
      null,
      null,
      1,
      0,
      0,
      fooVersion,
      fooVersion,
      fooVersion
    );
  });

  describe("related to gare", () => {
    it("addGare add a gare into gare list", () => {
      let sut = new Region(0, foo, foo, fooVersion, fooVersion);

      sut.addGare(gare);

      expect(sut.gares.length).toBe(1);
      expect(sut.gares[0]).toBe(gare);
    });

    it("getGare returns undefined whe the gare doesn't exist", () => {
      let region = new Region(0, foo, foo, fooVersion, fooVersion);
      region.addGare(gare);

      let sut = region.getGare(bar);

      expect(sut).toBeUndefined();
    });

    it("getGare returns the gare", () => {
      let region = new Region(0, foo, foo, fooVersion, fooVersion);
      region.addGare(gare);

      let sut = region.getGare(gare.idExterne);

      expect(sut).toBe(gare);
    });
  });

  describe("related to line", () => {
    it("addLigne add a line into line list", () => {
      let sut = new Region(0, foo, foo, fooVersion, fooVersion);

      sut.addLigne(ligne);

      expect(sut.lignes.length).toBe(1);
      expect(sut.lignes[0]).toBe(ligne);
    });

    it("getLigne returns undefined when the line doesn't exists", () => {
      let region = new Region(0, foo, foo, fooVersion, fooVersion);
      region.addLigne(ligne);

      let sut = region.getLigne(bar);

      expect(sut).toBeUndefined();
    });

    it("getLigne returns the line", () => {
      let region = new Region(0, foo, foo, fooVersion, fooVersion);
      region.addLigne(ligne);

      let sut = region.getLigne(ligne.idExterne);

      expect(sut).toBe(ligne);
    });
  });

  describe("related to Gare dans ligne", () => {
    it("addGareDansLigne add a gdl into gdl list", () => {
      let sut = new Region(0, foo, foo, fooVersion, fooVersion);

      sut.addGaresDansLigne(gdl);

      expect(sut.garesdansligne.length).toBe(1);
      expect(sut.garesdansligne[0]).toBe(gdl);
    });

    it("addGareDansLigne doesn't add the same gdl twice", () => {
      let fooVersion2 = new Version(2, true);

      let gdl2 = new GareDansLigne(
        gare,
        ligne,
        null,
        null,
        null,
        42,
        2,
        2,
        fooVersion2,
        fooVersion2,
        fooVersion2
      );

      let sut = new Region(0, foo, foo, fooVersion, fooVersion);

      sut.addGaresDansLigne(gdl);
      sut.addGaresDansLigne(gdl2);

      expect(sut.garesdansligne.length).toBe(1);
      expect(sut.garesdansligne[0]).toBe(gdl);
    });
  });

  it("extractData returns correct csv", () => {
    let newVersion = 999;
    let nonExsistVersion = new Version(42, false);
    let newGare = new Gare(
      "newGare",
      "newGare",
      foo,
      foo,
      0,
      0,
      Couleur.Cyan,
      Couleur.Cyan,
      nonExsistVersion,
      fooVersion,
      fooVersion
    );
    let newLigne = new Ligne(
      "newLine",
      foo,
      TypeLigne.Funiculaire,
      0,
      foo,
      nonExsistVersion,
      fooVersion,
      fooVersion
    );
    let newGdl = new GareDansLigne(
      newGare,
      newLigne,
      null,
      null,
      null,
      1,
      0,
      0,
      nonExsistVersion,
      fooVersion,
      fooVersion
    );

    let region = new Region(0, foo, foo, fooVersion, fooVersion);

    region.addGare(gare);
    region.addGare(newGare);
    region.addLigne(ligne);
    region.addLigne(newLigne);
    region.addGaresDansLigne(gdl);
    region.addGaresDansLigne(newGdl);

    let sut = region.generateExportData(newVersion);

    expect(sut.fileLignes).toBe(
      "idExterne;nom;type;ordre;couleur;vCreation;vMaj;vSuppression" +
        "\r\n" +
        "fooIdLigne;" +
        foo +
        ";Funiculaire;0;" +
        foo +
        ";1;1;1" +
        "\n" +
        "newLine;" +
        foo +
        ";Funiculaire;0;" +
        foo +
        ";999;1;1"
    );

    expect(sut.fileGares).toBe(
      "idExterne;nom;exploitant;latitude;longitude;couleur;couleurEvolution;vCreation;vMaj;vSuppression" +
        "\r\n" +
        "fooIdGare;" +
        foo +
        ";" +
        foo +
        ";0;0;Cyan;Cyan" +
        ";1;1;1" +
        "\n" +
        "newGare;" +
        foo +
        ";" +
        foo +
        ";0;0;Cyan;Cyan" +
        ";999;1;1"
    );

    expect(sut.fileGaresDansLigne).toBe(
      "idGare;idLigne;nom;surTitre;sousTitre;ordre;PDLFond;PDLPoint;vCreation;vMaj;vSuppression" +
        "\r\n" +
        "fooIdGare;fooIdLigne;;;;1;0;0;1;1;1" +
        "\n" +
        "newGare;newLine;;;;1;0;0;999;1;1"
    );
  });
});
