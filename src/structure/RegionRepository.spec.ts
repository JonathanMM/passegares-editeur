import { RegionRepository } from "./RegionRepository";
import Region from "./Region";

describe("RegionRepository", () => {
  it("the constructor works", () => {
    let regions = new Array<Region>();

    let sut = new RegionRepository(regions);

    expect(sut.regions).toBe(regions);
  });
});
