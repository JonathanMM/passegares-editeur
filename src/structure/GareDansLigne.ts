import Gare from "./Gare";
import Ligne from "./Ligne";
import { Fond, Point } from "./PointDansLigne";
import Version from "./Version";
import { VersionnedItem } from "./VersionnedItem";

export default class GareDansLigne extends VersionnedItem {
  gare: Gare;
  ligne: Ligne;
  nom: string | null;
  surTitre: string | null;
  sousTitre: string | null;
  ordre: number;
  pdlFond: Fond;
  pdlPoint: Point;

  constructor(
    gare: Gare,
    ligne: Ligne,
    nom: string | null,
    surTitre: string | null,
    sousTitre: string | null,
    ordre: number,
    pdlFond: number,
    pdlPoint: number,
    vCreation: Version,
    vMaj: Version,
    vSuppression: Version | null //Ne doit pas être nul
  ) {
    super(vCreation, vMaj, vSuppression);
    this.gare = gare;
    this.ligne = ligne;
    this.nom = nom;
    this.surTitre = surTitre;
    this.sousTitre = sousTitre;
    this.ordre = ordre;
    this.pdlFond = pdlFond;
    this.pdlPoint = pdlPoint;
  }

  public withOrdre(ordre: number): GareDansLigne {
    return new GareDansLigne(
      this.gare,
      this.ligne,
      this.nom,
      this.surTitre,
      this.sousTitre,
      ordre,
      this.pdlFond,
      this.pdlPoint,
      this.versionCreation,
      this.versionMaj,
      this.versionSuppression
    );
  }

  public withFond(fond: Fond): GareDansLigne {
    return new GareDansLigne(
      this.gare,
      this.ligne,
      this.nom,
      this.surTitre,
      this.sousTitre,
      this.ordre,
      fond,
      this.pdlPoint,
      this.versionCreation,
      this.versionMaj,
      this.versionSuppression
    );
  }

  public withPoint(point: Point): GareDansLigne {
    return new GareDansLigne(
      this.gare,
      this.ligne,
      this.nom,
      this.surTitre,
      this.sousTitre,
      this.ordre,
      this.pdlFond,
      point,
      this.versionCreation,
      this.versionMaj,
      this.versionSuppression
    );
  }
}
